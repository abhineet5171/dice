/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game
*/
var score, roundScore, activePlayer;
score = [0,0];
roundScore = 0;
activePlayer=0;
document.querySelector('.dice').style.display = 'none';
//document.querySelector('score-0').textContent= ;
//document.querySelector('score-1').textContent='0';
//*************************ROLL******************//
document.querySelector('.btn-roll').addEventListener('click', function(){
//1. random number
var dice = Math.floor(Math.random()*6)+1;
//2. Display result
document.querySelector('.dice').style.display = 'block' ;
document.querySelector('.dice').src = './resources/'+'dice-'+dice + '.png';
//3. Implement roundScore
    if (dice>1){roundScore += dice;
    document.querySelector('#current-'+ activePlayer).textContent= roundScore;
    }
      else {roundScore=0;
        document.querySelector('#current-'+ activePlayer).textContent= roundScore;
            if (activePlayer==0){activePlayer=1;}
            else {activePlayer=0;}
            }
document.querySelector('#current-'+ activePlayer).textContent= roundScore;
});
//***********************ROLL******************//



//**********hold***********//

document.querySelector('.btn-hold').addEventListener('click',function(){
  //**add to score**//
      score[activePlayer] += roundScore;
      if (score[0]>=100){
        alert('player 1 is winner');
        newGame();
      }
      else if (score[1]>=100){alert ('player 2 is winner');
      newGame();}
  //**make roundscore =0 and show that in current **//
      roundScore=0;
      document.querySelector('#current-'+ activePlayer).textContent= roundScore;
  //**change active player**//
      document.querySelector('#score-'+ activePlayer).textContent= score[activePlayer];
      if (activePlayer==0){activePlayer=1;}
      else {activePlayer=0;}

})

//**********new**********//
function newGame(){
score[0]=0;
score[1]=0;
roundScore=0;
activePlayer=0;
/*document.querySelector('#current-0')=score[0];
document.querySelector('#current-1')=score[1];
document.querySelector('#score-0')=roundScore;
document.querySelector('#score-1')=roundScore;
*/
document.querySelector('#current-0').textContent= roundScore;
document.querySelector('#current-1').textContent= roundScore;
document.querySelector('#score-0').textContent= roundScore;
document.querySelector('#score-1').textContent= roundScore;
document.querySelector('.dice').style.display = 'none';

}
document.querySelector('.btn-new').addEventListener('click',newGame)
